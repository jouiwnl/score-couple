import { CircularProgress } from '@mui/material';
import _ from 'lodash';
import React from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import { toast } from 'react-toastify';
import AddColumnButton from '../../components/AddColumnButton';
import Column from '../../components/Column';
import ColumnDialog from '../../components/ColumnDialog'
import { httpApi } from '../../config'
import { auth } from '../../firebase'

import { WorkSpaceWrapper, WorkSpaceColumns, LoadingWrapper } from './styles'

export default function({ reload }) {

  const [columns, setColumns] = React.useState([])
  const [isLoading, setIsLoading] = React.useState(false)
  const [openDialog, setOpenDialog] = React.useState(false)

  async function createWorkSpace(user) {
    var promise = httpApi.post(`/workspaces/${user.id}`);
    toast.promise(
      promise, 
      {
        pending: 'Criando seu espaço de filmes...',
        success: 'Seu espaço de filmes está pronto!',
        error: 'Houve um erro ao criar seu perfil, tente novamente!'
      }
    ).then(() => {
      getAllColumnsByWorkspace();
    })
  }

  async function getUser() {
    const user = await httpApi.get(`/users/${auth.currentUser.email}`).then(response => response.data);

    return user;
  }

  const getAllColumnsByWorkspace = () => {
    setColumns([])
    setIsLoading(true)
    setTimeout(() => {
      getUser().then((user) => {
        if (_.isEmpty(user.workspace)) {
          createWorkSpace(user);
        } else {
          setColumns(user.workspace.colunas);
        }
      }).then(() => {
        setIsLoading(false)
      })
    }, 750)
  }

  const handleDragEnd = async (e) => {
    if (e.reason != 'DROP' || !e.destination) {
      return;
    }

    const { source, destination } = e;
    const movieId = e.draggableId;

    if (source.droppableId != destination.droppableId) {
      const sourceColumn = _.find(columns, { id: Number(source.droppableId.split('.')[0]) });
      const destColumn = _.find(columns, { id: Number(destination.droppableId.split('.')[0]) });
      const sourceItems = [...sourceColumn.movies];
      const destItems = [...destColumn.movies];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);

      setColumns({
        ...columns,
        [source.droppableId.split('.')[1]]: {
          ...sourceColumn,
          movies: sourceItems
        },
        [destination.droppableId.split('.')[1]]: {
          ...destColumn,
          movies: destItems
        }
      });

      const movie = await httpApi.get(`/movies/${movieId}`).then(response => response.data);
      movie.columnId = Number(destination.droppableId);
      httpApi.put(`/movies/${movie.id}`, movie);
    }
  }
  
  React.useEffect(() => {
    getAllColumnsByWorkspace();
  }, [reload])

  const handleOpenDialog = () => {
    setOpenDialog(true)
  }

  const handleCloseDialog = (column) => {
    if (column) {
      getAllColumnsByWorkspace()
    }
    setOpenDialog(false)
  }

  return (
    <>
      <WorkSpaceWrapper>
        {!columns.length && isLoading ? (
          <LoadingWrapper>
            <CircularProgress sx={{ color: '#fff' }} size={60} disableShrink />
          </LoadingWrapper>
        ) : ''}

        {!isLoading ? (
          <WorkSpaceColumns>
            <DragDropContext onDragEnd={handleDragEnd}>
              { columns.length ? (
                columns.map((column, index) => (
                  <div key={String(Math.random())}>
                    <Column index={index} column={column} reload={getAllColumnsByWorkspace}/>
                  </div>
                ))
              ) : (
                Object.entries(columns).map(([ columnId, column ], index) => (
                  <div key={String(Math.random())}>
                    <Column index={index} column={column} reload={getAllColumnsByWorkspace}/>
                  </div>
                ))
              ) }
              
            </DragDropContext>
            <div style={{ height: '40px'}} onClick={handleOpenDialog}>
              <AddColumnButton />
            </div>
          </WorkSpaceColumns>
        ) : ''}

      </WorkSpaceWrapper>
      <ColumnDialog open={openDialog} onClose={handleCloseDialog}/>
    </>
  )
}