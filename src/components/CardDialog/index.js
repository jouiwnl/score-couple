import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import Slide from '@mui/material/Slide';
import Rating from '@mui/material/Rating';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import axios from 'axios'
import { API_KEY, httpApi } from '../../config'
import _ from 'lodash'
import ConfettiExplosion from 'react-confetti-explosion';
import {auth} from '../../firebase'

import { 
  CardWrapper, 
  LeftWrapper, 
  RightWrapper, 
  RightSideHeader, 
  Wrapper, 
  MovieInformation, 
  Information,
  MovieAvaliationDescription,
  ButtonWrapper,
  StatusWrapper,
  LoadingWrapper,
  NotFoundWrapper,
  MovieExistIndicatorWrapper,
  MovieExistIndicator,
  MovieExistIndicatorLabel
} from './styles';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { CircularProgress } from '@mui/material';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function ({ open, onClose, movie, columnid, isShuffled }) {
  const navigate = useNavigate();
  const [selectedMovie, setSelectedMovie] = React.useState(null);
  const [movieValues, setMovieValues] = React.useState(null);
  const [isSaving, setIsSaving] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [hasMovie, setHasMovie] = React.useState(false);
  const [user, setUser] = React.useState({});

  const handleClose = () => {
    onClose();
  };
  
  React.useEffect(() => {
    getUser();
  }, [])

  React.useEffect(() => {
    setIsLoading(true)
    setSelectedMovie(null)
    setMovieValues(null)
    if (_.isString(movie)) {
      setIsLoading(false);
      return;
    }

    if (movie) {
      if (movie != undefined) {
        if (_.isNumber(movie)) {
          const API_URL_ID = `https://api.themoviedb.org/3/movie/${movie}?api_key=${API_KEY}&query=`;
          axios.get(API_URL_ID).then((response) => {
            setSelectedMovie(response.data)
            hasAnyMovie(user.workspace.colunas, response.data)
          }).finally(() => {
            setIsLoading(false)
          });
          return true;
        }

        if (!_.isEmpty(movie)) {
          httpApi.get(`/movies/${movie.id}`).then((response) => {
            setMovieValues(response.data);
          }).finally(() => {
            setIsLoading(false)
          });
        }
      }
    }
  }, [movie])

  function hasAnyMovie(colunas, filme) {
    let allMovies = [];
    colunas.forEach(coluna => coluna.movies.forEach(insideMovie => allMovies.push(insideMovie)));
    console.log(allMovies.some(insideMovie => insideMovie.name === filme.title))
    setHasMovie(allMovies.some(insideMovie => insideMovie.name === filme.title));
  }

  const handleRegisterMovie = () => {
    const movie = {
      name: selectedMovie.title,
      posterUrl: selectedMovie.poster_path,
      releaseDate: selectedMovie.release_date,
      runtime: selectedMovie.runtime,
      genre: (selectedMovie.genres.length ? selectedMovie.genres[0].name : ''),
      movieDescription: selectedMovie.overview.split('.')[0]
    }

    setIsSaving(true)

    const promise = httpApi.post(`/movies?columnid=${columnid}`, movie).then(() => {
      navigate('/')
    }).finally(() => {
      setIsSaving(false)
    })

    toast.promise(
      promise,
      {
        pending: 'Salvando filme',
        success: 'Filme salvo com sucesso!',
        error: 'Erro ao salvar filme!'
      }
    );
  }

  const handleInputChange = (e) => {
    const {name, value} = e.target;
    setMovieValues({...movieValues, [name]: value});
  }

  const handleAvaliation = (aval) => {
    setMovieValues({...movieValues, score: aval})
  }

  const handleUpdate = () => {
    setIsSaving(true);
    const promise = httpApi.put(`/movies/${movieValues.id}`, movieValues).then(() => {
      onClose(movieValues)
    }).finally(() => {
      setIsSaving(false);
    })

    toast.promise(
      promise,
      {
        pending: 'Salvando registro',
        success: 'Registro salvo com sucesso!',
        error: 'Erro ao salvar registro!'
      }
    );
  }

  const handleDelete = () => {
    setIsSaving(true)
    const promise = httpApi.delete(`/movies/${movieValues.id}`).then((response) => {
      onClose(movieValues)
    }).finally(() => {
      setIsSaving(false)
    })

    toast.promise(
      promise,
      {
        pending: 'Deletando card...',
        success: 'Card deletado com sucesso!',
        error: 'Erro ao deletar o card!'
      }
    )
  }

  async function getUser() {
    await httpApi.get(`/users/${auth.currentUser.email}`).then(response => {
      setUser(response.data)
    });
  }

  const setImage = () => {
    if (!_.isEmpty(selectedMovie)) {
      return `https://image.tmdb.org/t/p/w400${selectedMovie.poster_path}`;
    }

    if (!_.isEmpty(movieValues)) {
      return `https://image.tmdb.org/t/p/w400${movieValues.posterUrl}`
    }

    return '';
  }

  const theme = createTheme({
    palette: {
      primary: {
        main: '#faaf00',
        darker: '#053e85',
      },
      completed: {
        main: '#98FB98',
        darker: '#053e85',
      },
      canceled: {
        main: '#FA8072',
        darker: '#053e85',
      },
      doing: {
        main: '#FF8C00',
        darker: '#053e85'
      },
      notstarted: {
        main: '#464646',
        darker: '#053e85',
      }
    },
  });

  function defineTheme() {
    if (movieValues.status == 'COMPLETED') {
      return 'completed'
    }

    if (movieValues.status == 'DOING') {
      return 'doing'
    }

    if (movieValues.status == 'CANCELED') {
      return 'canceled'
    }

    return 'primary'
  }

  return (
    <Wrapper>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        onClose={handleClose}
        maxWidth={'1200px'}
        id="dialogCard"
      > 
        {isShuffled ? <ConfettiExplosion /> : ''}

        {isLoading ? (
          <LoadingWrapper>
            <CircularProgress sx={{ color: '#faaf00' }} size={60} disableShrink />
          </LoadingWrapper>
        ) : ''}

        
          {movieValues || selectedMovie ? (
            <CardWrapper>
              <LeftWrapper image={setImage()}/>
              <RightWrapper>
                <RightSideHeader>
                  { selectedMovie ? selectedMovie.title : movieValues.name }
                </RightSideHeader>

                <MovieInformation>
                  <Information>
                    { selectedMovie ? selectedMovie.release_date : movieValues.releaseDate  }
                  </Information>
                  <Information>
                    { selectedMovie ? selectedMovie.runtime : movieValues.runtime } min
                  </Information>
                  <Information>
                    { selectedMovie ? (selectedMovie.genres.length ? selectedMovie.genres[0].name : '') : movieValues.genre }
                  </Information>
                </MovieInformation>
                {movieValues ? (
                  <>
                    <MovieAvaliationDescription>
                        <span style={{ color: '#fff', paddingLeft: '20px', paddingRight: '20px',  paddingTop: '15px' }}>
                          {movieValues.movieDescription}.
                        </span>
                      <StatusWrapper>
                        <ThemeProvider theme={theme}>
                          <Select
                            value={movieValues.status || ''}
                            name="status"
                            onChange={handleInputChange}
                            fullWidth
                            color={defineTheme()}
                            sx={{ color: '#fff', fontWeight: '700', maxWidth: '200px', height: '40px' }}
                            className="Mui-focused"
                          >
                            <MenuItem value={'NOTSTARTED'}>A fazer</MenuItem>
                            <MenuItem value={'CANCELED'}>Cancelado</MenuItem>
                            <MenuItem value={'COMPLETED'}>Feito</MenuItem>
                            <MenuItem value={'DOING'}>Fazendo</MenuItem>
                          </Select>
                        </ThemeProvider>
                      </StatusWrapper>
                      { movieValues.status !== "NOTSTARTED" ? (
                        <Rating
                          name="simple-controlled"
                          value={movieValues.score || 0}
                          onChange={(e, newVal) => handleAvaliation(newVal)}
                          sx={{ label: { span: { color: '#faaf00' } }, marginTop: '23px' }}
                        />
                      ) : ''}
                    </MovieAvaliationDescription>
                    <ButtonWrapper>
                      <ThemeProvider theme={theme}>
                        <Button onClick={handleUpdate} disabled={isSaving} variant="outlined" color="primary" sx={{ color: '#FFF' }}>Salvar</Button>
                        <Button onClick={handleDelete} disabled={isSaving} variant="outlined" color="primary" sx={{ color: '#FFF' }}>Apagar</Button>
                      </ThemeProvider>
                    </ButtonWrapper>
                  </>
                ) : (
                  <>
                    <MovieAvaliationDescription style={{ color: '#fff', padding: '30px' }}>
                      <span>
                        {selectedMovie.overview}
                      </span>
                    </MovieAvaliationDescription>
                    <ButtonWrapper>
                      <ThemeProvider theme={theme}>
                        {hasMovie ? (
                          <MovieExistIndicatorWrapper>
                            <MovieExistIndicator>
                              <MovieExistIndicatorLabel>Já adicionado</MovieExistIndicatorLabel>
                            </MovieExistIndicator>
                          </MovieExistIndicatorWrapper>
                        ) : (
                          <Button 
                            variant="outlined" 
                            color="primary" 
                            sx={{ color: '#FFF', marginBottom: '13px' }}
                            onClick={handleRegisterMovie}
                            disabled={isSaving}
                          >
                            Adicionar
                          </Button>
                        )}
                      </ThemeProvider>
                    </ButtonWrapper>
                  </>
                )}
              </RightWrapper>
            </CardWrapper>
          ) : ''}

          {_.isString(movie) ? (
            <NotFoundWrapper>
              <h1>Nenhum filme foi sorteado :( </h1>
            </NotFoundWrapper>
          ) : ''}        
      </Dialog>
    </Wrapper>
  );
}
