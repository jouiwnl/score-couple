import React from 'react';

import { HeaderWrapper, HeaderActionsWrapper, Logo, HeaderButton, UserInformation } from './styles'
import logo from '../../assets/logo.svg'
import icon from '../../assets/icon.png'
import { Link } from 'react-router-dom';
import { signOut } from 'firebase/auth'
import { auth } from '../../firebase'
import HomeIcon from '@mui/icons-material/Home';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import LogoutIcon from '@mui/icons-material/Logout'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { httpApi } from '../../config'
import { Button, createTheme, ThemeProvider } from '@mui/material';
import CardDialog from '../CardDialog';

export default function({ reload }) {
	const [width, setWidth] = React.useState(window.innerWidth)
	const [user, setUser] = React.useState("");
	const [selectedMovie, setSelectedMovie] = React.useState({});
	const [openDialog, setOpenDialog] = React.useState(false);
	const [isExploding, setIsExploding] = React.useState(false);

  const handleOpenDialog = (movie) => {
		if (movie) {
			setSelectedMovie(movie);
		}
    setOpenDialog(true)
  }

  const handleClose = (registro) => {
		if (registro) {
			reload()
		}
    setOpenDialog(false);
  };

	React.useEffect(() => {
		getUser();
	}, []);
	
	setInterval(() => {
		setWidth(window.innerWidth)
	}, 1500)

	async function getUser() {
		httpApi.get(`/users/${auth.currentUser.email}`).then(response => {
			const user = response.data;
			setUser(user);
		})
	} 

	async function handleSortear() {
		setSelectedMovie(null)
		setOpenDialog(true)
		httpApi.get(`/workspaces/${user.workspace.id}/shuffle`).then(response => {
			setSelectedMovie(response.data)
			return response.data;
		}).then((movie) => {
			if (_.isObject(movie)) {
				setIsExploding(true)
				setTimeout(() => {
					setIsExploding(false)
				}, 3000)
			}
		})
	}
	
	const handleSignOut = () => {
		const promise = new Promise((resolve, reject) => {
			setTimeout(() => {
				resolve(signOut(auth));
			}, 1000)
		});
		
		toast.promise(
			promise,
			{
				pending: 'Deslogando...',
				error: 'Erro ao deslogar!'
			}
		)
	}

	const theme = createTheme({
    palette: {
      primary: {
        main: '#ff3390',
        darker: '#053e85',
      }
    },

  });

	return (
		<>
			<header>
				<HeaderWrapper>
					<HeaderActionsWrapper>
						<Logo>
							<img src={width < 600 ? icon : logo} alt="logo"></img>
						</Logo>
						<HeaderButton>
							<Link to="/" style={{ color: '#FFF', textDecoration: 'none', display: 'flex', alignItems: 'center' }}>
								<HomeIcon/>
								Home
							</Link>
						</HeaderButton>
						<HeaderButton>
							<ThemeProvider theme={theme}>
								<Button onClick={handleSortear} title='Sortear' color='primary' variant="contained" sx={{ color: '#fff', heigth: '10px', marginLeft: '7px', padding: '3px', minWidth: '50px' }}>
									<CardGiftcardIcon />
								</Button>
							</ThemeProvider>
						</HeaderButton>
					</HeaderActionsWrapper>
					<UserInformation>
						<span style={{ marginRight: '10px' }}>
							{user.username}
						</span>
						<div onClick={handleSignOut} style={{ cursor: 'pointer' }}>
							<LogoutIcon />
						</div>
					</UserInformation>
				</HeaderWrapper>
			</header>
			<CardDialog isShuffled={isExploding} open={openDialog} onClose={handleClose} movie={selectedMovie}/>
		</>
	)

}