import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { auth } from './firebase'

let app = '';

auth.onAuthStateChanged(() => {
  if (!app) {
    ReactDOM.render(
      <React.StrictMode>
        <App />
      </React.StrictMode>,
      document.getElementById('root')
    );
  }
})
